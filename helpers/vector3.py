from __future__ import annotations


class Vector3:
    x = 0
    y = 0
    z = 0

    def __init__(self, vec=None) -> None:
        if vec is not None and len(vec) == 3:
            self.x = vec[0]
            self.y = vec[1]
            self.z = vec[2]

    def __str__(self) -> str:
        return "x: " + str(self.x) + " y: " + str(self.y) + " z: " + str(self.z)
