from __future__ import annotations
from math import asin, atan2, pi

from helpers.vector3 import Vector3


class Matrix4x4:
    data = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

    def __init__(self: Matrix4x4, m=None) -> None:
        if (
            m is not None
            and len(m) == 4
            and len(m[0]) == 4
            and len(m[1]) == 4
            and len(m[2]) == 4
            and len(m[3]) == 4
        ):
            self.data = m

    def __mul__(self: Matrix4x4, other: Matrix4x4) -> Matrix4x4:
        # result is 3x4
        result = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

        a = self.data
        b = other.data

        # iterate through rows of a
        for i in range(len(a)):
            # iterate through columns of b
            for j in range(len(b[0])):
                # iterate through rows of b
                for k in range(len(b)):
                    result[i][j] += a[i][k] * b[k][j]

        return Matrix4x4(result)

    def __str__(self) -> str:
        return str(self.data)

    def GetAngles(self: Matrix4x4) -> Vector3:
        d = self.data
        thetaX = asin(d[2][1])
        thetaY = 0.0
        thetaZ = 0.0

        if thetaX < pi / 2:
            if thetaX > -pi / 2:
                thetaZ = atan2(-d[0][1], d[1][1])
                thetaY = atan2(-d[2][0], d[2][2])
            else:
                thetaZ = -atan2(-d[0][2], d[0][0])
                thetaY = 0
        else:
            thetaZ = atan2(d[0][2], d[0][0])
            thetaY = 0

        # Create return object.
        angles = Vector3([thetaX, thetaY, thetaZ])

        return angles

    def Invert(self: Matrix4x4) -> Matrix4x4:

        m = self.data

        inv = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]
        invOut = [[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]]

        inv[0][0] = (
            m[1][1] * m[2][2] * m[3][3]
            - m[1][1] * m[2][3] * m[3][2]
            - m[2][1] * m[1][2] * m[3][3]
            + m[2][1] * m[1][3] * m[3][2]
            + m[3][1] * m[1][2] * m[2][3]
            - m[3][1] * m[1][3] * m[2][2]
        )

        inv[1][0] = (
            -m[1][0] * m[2][2] * m[3][3]
            + m[1][0] * m[2][3] * m[3][2]
            + m[2][0] * m[1][2] * m[3][3]
            - m[2][0] * m[1][3] * m[3][2]
            - m[3][0] * m[1][2] * m[2][3]
            + m[3][0] * m[1][3] * m[2][2]
        )

        inv[2][0] = (
            m[1][0] * m[2][1] * m[3][3]
            - m[1][0] * m[2][3] * m[3][1]
            - m[2][0] * m[1][1] * m[3][3]
            + m[2][0] * m[1][3] * m[3][1]
            + m[3][0] * m[1][1] * m[2][3]
            - m[3][0] * m[1][3] * m[2][1]
        )

        inv[3][0] = (
            -m[1][0] * m[2][1] * m[3][2]
            + m[1][0] * m[2][2] * m[3][1]
            + m[2][0] * m[1][1] * m[3][2]
            - m[2][0] * m[1][2] * m[3][1]
            - m[3][0] * m[1][1] * m[2][2]
            + m[3][0] * m[1][2] * m[2][1]
        )

        inv[0][1] = (
            -m[0][1] * m[2][2] * m[3][3]
            + m[0][1] * m[2][3] * m[3][2]
            + m[2][1] * m[0][2] * m[3][3]
            - m[2][1] * m[0][3] * m[3][2]
            - m[3][1] * m[0][2] * m[2][3]
            + m[3][1] * m[0][3] * m[2][2]
        )

        inv[1][1] = (
            m[0][0] * m[2][2] * m[3][3]
            - m[0][0] * m[2][3] * m[3][2]
            - m[2][0] * m[0][2] * m[3][3]
            + m[2][0] * m[0][3] * m[3][2]
            + m[3][0] * m[0][2] * m[2][3]
            - m[3][0] * m[0][3] * m[2][2]
        )

        inv[2][1] = (
            -m[0][0] * m[2][1] * m[3][3]
            + m[0][0] * m[2][3] * m[3][1]
            + m[2][0] * m[0][1] * m[3][3]
            - m[2][0] * m[0][3] * m[3][1]
            - m[3][0] * m[0][1] * m[2][3]
            + m[3][0] * m[0][3] * m[2][1]
        )

        inv[3][1] = (
            m[0][0] * m[2][1] * m[3][2]
            - m[0][0] * m[2][2] * m[3][1]
            - m[2][0] * m[0][1] * m[3][2]
            + m[2][0] * m[0][2] * m[3][1]
            + m[3][0] * m[0][1] * m[2][2]
            - m[3][0] * m[0][2] * m[2][1]
        )

        inv[0][2] = (
            m[0][1] * m[1][2] * m[3][3]
            - m[0][1] * m[1][3] * m[3][2]
            - m[1][1] * m[0][2] * m[3][3]
            + m[1][1] * m[0][3] * m[3][2]
            + m[3][1] * m[0][2] * m[1][3]
            - m[3][1] * m[0][3] * m[1][2]
        )

        inv[1][2] = (
            -m[0][0] * m[1][2] * m[3][3]
            + m[0][0] * m[1][3] * m[3][2]
            + m[1][0] * m[0][2] * m[3][3]
            - m[1][0] * m[0][3] * m[3][2]
            - m[3][0] * m[0][2] * m[1][3]
            + m[3][0] * m[0][3] * m[1][2]
        )

        inv[2][2] = (
            m[0][0] * m[1][1] * m[3][3]
            - m[0][0] * m[1][3] * m[3][1]
            - m[1][0] * m[0][1] * m[3][3]
            + m[1][0] * m[0][3] * m[3][1]
            + m[3][0] * m[0][1] * m[1][3]
            - m[3][0] * m[0][3] * m[1][1]
        )

        inv[3][2] = (
            -m[0][0] * m[1][1] * m[3][2]
            + m[0][0] * m[1][2] * m[3][1]
            + m[1][0] * m[0][1] * m[3][2]
            - m[1][0] * m[0][2] * m[3][1]
            - m[3][0] * m[0][1] * m[1][2]
            + m[3][0] * m[0][2] * m[1][1]
        )

        inv[0][3] = (
            -m[0][1] * m[1][2] * m[2][3]
            + m[0][1] * m[1][3] * m[2][2]
            + m[1][1] * m[0][2] * m[2][3]
            - m[1][1] * m[0][3] * m[2][2]
            - m[2][1] * m[0][2] * m[1][3]
            + m[2][1] * m[0][3] * m[1][2]
        )

        inv[1][3] = (
            m[0][0] * m[1][2] * m[2][3]
            - m[0][0] * m[1][3] * m[2][2]
            - m[1][0] * m[0][2] * m[2][3]
            + m[1][0] * m[0][3] * m[2][2]
            + m[2][0] * m[0][2] * m[1][3]
            - m[2][0] * m[0][3] * m[1][2]
        )

        inv[2][3] = (
            -m[0][0] * m[1][1] * m[2][3]
            + m[0][0] * m[1][3] * m[2][1]
            + m[1][0] * m[0][1] * m[2][3]
            - m[1][0] * m[0][3] * m[2][1]
            - m[2][0] * m[0][1] * m[1][3]
            + m[2][0] * m[0][3] * m[1][1]
        )

        inv[3][3] = (
            m[0][0] * m[1][1] * m[2][2]
            - m[0][0] * m[1][2] * m[2][1]
            - m[1][0] * m[0][1] * m[2][2]
            + m[1][0] * m[0][2] * m[2][1]
            + m[2][0] * m[0][1] * m[1][2]
            - m[2][0] * m[0][2] * m[1][1]
        )

        det = m[0][0] * inv[0][0] + m[0][1] * inv[1][0] + m[0][2] * inv[2][0] + m[0][3] * inv[3][0]

        if det == 0:
            return None

        det = 1.0 / det

        for i in range(0, 16):
            invOut[i // 4][i % 4] = inv[i // 4][i % 4] * det

        return Matrix4x4(invOut)

    def ToText(self: Matrix4x4) -> str:

        d = self.data

        text = ""
        for i in range(len(d)):
            for j in range(len(d[i])):
                text += str(d[i][j]) + " "
        return text.removesuffix(" ")

    @staticmethod
    def TextToMatrix4x4(text: str) -> Matrix4x4:
        splited = text.split(" ")

        if len(splited) != 16:
            return []

        out = Matrix4x4()
        out.data = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]]

        for i in range(16):
            out.data[i // 4][i % 4] = float(splited[i])

        return out
