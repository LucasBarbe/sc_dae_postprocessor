# SC Dae PostProcessor

Post process dae file from cgf converter to add bone extra for Blender and fix skin data

## Usage

```powershell
python3 scdaepostprocessor.py "inout.dae"
```

```powershell
python3 scdaepostprocessor.py "in.dae" -o "out.dae"
```
