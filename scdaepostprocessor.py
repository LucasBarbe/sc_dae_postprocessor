from math import pi
from pathlib import Path
import xml.etree.ElementTree as ET
from helpers.matrix4x4 import Matrix4x4
from helpers.vector3 import Vector3

import argparse

COLLADA_PREFIX = "{http://www.collada.org/2005/11/COLLADASchema}"


def GetTranslateMatrixOne(axis: str = "Y"):
    tailMatrix = Matrix4x4([[1, 0, 0, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]])

    if axis == "X":
        tailMatrix = Matrix4x4([[1, 0, 0, 1], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

    if axis == "Z":
        tailMatrix = Matrix4x4([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 1], [0, 0, 0, 1]])

    return tailMatrix


def GetBoneTailPosition(m: Matrix4x4, axis: str = "Y") -> Vector3:

    tailMatrix = GetTranslateMatrixOne(axis)

    tail = m * tailMatrix

    return Vector3(
        [
            tail.data[0][3] - m.data[0][3],
            tail.data[1][3] - m.data[1][3],
            tail.data[2][3] - m.data[2][3],
        ]
    )


def GetBoneExtraFromMatrix(m: Matrix4x4) -> tuple[Vector3, float]:

    tailOffset = GetBoneTailPosition(m, "Y")
    angles = m.GetAngles()

    if angles.y == -pi and angles.z == -pi:
        angles.y = 0

    return tailOffset, angles.y


def InsertBlenderExtra(node: ET.Element, matrix: Matrix4x4, tailLength: float = 1):

    extra = node.find(COLLADA_PREFIX + "extra")
    if extra is None:
        extra = ET.Element(COLLADA_PREFIX + "extra")
        node.append(extra)

    technique = extra.find("{*}technique")
    if technique is None:
        technique = ET.Element(COLLADA_PREFIX + "technique", {"profile": "blender"})
        extra.append(technique)

    layer = technique.find("{*}layer")
    if layer is None:
        layer = ET.Element(COLLADA_PREFIX + "layer", {"sid": "layer", "type": "string"})
        technique.append(layer)

    layer.text = "0"

    tailOffset, rollValue = GetBoneExtraFromMatrix(matrix)

    if tailOffset.x == 0 and tailOffset.y == 0 and tailOffset.z == 0:
        tailOffset.y = 1

    tailOffset.x *= tailLength
    tailOffset.y *= tailLength
    tailOffset.z *= tailLength

    roll = technique.find("{*}roll")
    if roll is None:
        roll = ET.Element(COLLADA_PREFIX + "roll", {"sid": "roll", "type": "float"})
        technique.append(roll)

    roll.text = str(rollValue)

    tip_x = technique.find("{*}tip_x")
    if tip_x is None:
        tip_x = ET.Element(COLLADA_PREFIX + "tip_x", {"sid": "tip_x", "type": "float"})
        technique.append(tip_x)

    tip_x.text = str(tailOffset.x)

    tip_y = technique.find("{*}tip_y")
    if tip_y is None:
        tip_y = ET.Element(COLLADA_PREFIX + "tip_y", {"sid": "tip_y", "type": "float"})
        technique.append(tip_y)

    tip_y.text = str(tailOffset.y)

    tip_z = technique.find("{*}tip_z")
    if tip_z is None:
        tip_z = ET.Element(COLLADA_PREFIX + "tip_z", {"sid": "tip_z", "type": "float"})
        technique.append(tip_z)

    tip_z.text = str(tailOffset.z)


def ProcessNodesRecursivly(
    nodeElement: ET.Element, parentGlobalMatrix: Matrix4x4, tailLength: float = 1, matrixDic=None
):
    childNodes = nodeElement.findall(COLLADA_PREFIX + "node")
    matrixElement = nodeElement.find(COLLADA_PREFIX + "matrix")

    matrix = Matrix4x4.TextToMatrix4x4(matrixElement.text)

    matrix = parentGlobalMatrix * matrix

    InsertBlenderExtra(nodeElement, matrix, tailLength)

    if matrixDic is not None:
        matrixDic[nodeElement.get("sid")] = matrix

    for childNode in childNodes:
        ProcessNodesRecursivly(childNode, matrix, tailLength, matrixDic)


def AddPostPorcessInfo(rootElement: ET.Element, value: SyntaxWarning):
    assetElement = rootElement.find("{*}asset")
    contribrutorElement = assetElement.find("{*}contributor")

    authoringToolElement = rootElement.find("{*}authoring_tool")
    if authoringToolElement is None:
        authoringToolElement = ET.Element("authoring_tool")
        contribrutorElement.append(authoringToolElement)

    authoringToolElement.set("LFusion", value)


def PostProcess(input: Path, output: Path):

    if not input.is_file() and input.suffix.lower() == ".dae":
        print("Error: " + str(input) + "is not a dae file")
        return

    ET.register_namespace("", "http://www.collada.org/2005/11/COLLADASchema")
    ET.register_namespace("xsi", "http://www.w3.org/2001/XMLSchema-instance")
    ET.register_namespace("xsd", "http://www.w3.org/2001/XMLSchema")

    tree = ET.parse(input)
    root = tree.getroot()

    root.set("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
    root.set("xmlns:xsd", "http://www.w3.org/2001/XMLSchema")
    root.tag.removesuffix("COLLADA")

    print("PostProcessing started on: " + input.name)
    rootNodeElement = root.find("./{*}library_visual_scenes/{*}visual_scene/{*}node")

    dic_boneName_matrix = {}

    extraAdded = False
    skinFixedData = False

    if rootNodeElement is not None:
        ProcessNodesRecursivly(rootNodeElement, Matrix4x4(), 0.05, dic_boneName_matrix)
        extraAdded = True

    controllerElement = root.find("./{*}library_controllers/{*}controller")

    if controllerElement is not None:
        controllerId = controllerElement.get("id")
        Name_arrayElement = controllerElement.find(
            "./{*}skin/{*}source[@id='" + controllerId + "-joints'" + "]/{*}Name_array"
        )
        if Name_arrayElement is not None:
            bonesNames = Name_arrayElement.text.split(" ")
            sourceElement = controllerElement.find(
                "./{*}skin/{*}source[@id='"
                + controllerId
                + "-bind_poses'"
                + "]/{*}float_array[@id='"
                + controllerId
                + "-bind_poses-array'"
                + "]"
            )
            if sourceElement is not None:
                sourceElement.text = ""
                for bName in bonesNames:
                    sourceElement.text += " " + dic_boneName_matrix[bName].Invert().ToText()
                skinFixedData = True

    if extraAdded or skinFixedData:
        info = "PostProcessed: "

        if extraAdded:
            info += "[Blender Extra]"
            print("\tNode data: Done")
        else:
            print("\tNode data: Skipped")

        if skinFixedData:
            info += "[Skin Data]"
            print("\tSkin data: Done")
        else:
            print("\tSkin data: Skipped")

        AddPostPorcessInfo(root, info)

        print("PostProcessing ended")

        ET.indent(tree, "  ")
        tree.write(output, encoding="utf-8", xml_declaration=True)
        print(output.name + " saved")


parser = argparse.ArgumentParser(
    prog="SC Dae Post Processor",
    description="Post Process dae file from cgf converter to add extra bone data for Blender and fix skin data",
)

parser.add_argument(
    "inPath",
    metavar="input",
    type=Path,
    help="All the numbers separated by spaces will be added.",
)

parser.add_argument(
    "-o",
    "-out",
    metavar="output",
    type=Path,
    help="All the numbers separated by spaces will be added.",
    required=False,
)

args = parser.parse_args()

inPath = args.inPath
outPath = inPath
if args.o is not None:
    outPath = args.o

PostProcess(inPath, outPath)
